#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

// semWait(int fd)
void internal_semWait(){
  disastrOS_debug("[semWait] entering\n");
  
  int fd = running->syscall_args[0];

  SemDescriptor* des = SemDescriptorList_byFd(&running->sem_descriptors, fd);

  if(! des){
  	running->syscall_retvalue=DSOS_ESEMWAIT;
  }

  Semaphore* sem = des->semaphore;
  assert(sem);

  if( sem->count > 0){
    sem->count--;
		running->syscall_retvalue = 0;
  }	
  else{
  	running->status=Waiting;
  	disastrOS_debug("[semWait] locking pcb %d\n", running->pid);
  	List_insert(&waiting_list, waiting_list.last, (ListItem*) running);
  	SemDescriptorPtr* desptr = (SemDescriptorPtr*) List_detach(&sem->descriptors, (ListItem*) des->ptr);
  	List_insert(&(sem->waiting_descriptors), sem->waiting_descriptors.last, (ListItem*) desptr);
  	
  	PCB* next_running= (PCB*) List_detach(&ready_list, ready_list.first);
  	running=next_running;
  	
  	disastrOS_debug("[semWait] new pcb running %d\n", running!=0?running->pid:-1);
  }
  disastrOS_debug("[semWait] semaphore %d waited, everything went right\n", sem->id);
}
