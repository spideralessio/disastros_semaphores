#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"


// semOpen(int id, int mode, unsigned int value);

void internal_semOpen(){
	// debug don't se nega a nessuno
	disastrOS_debug("[semOpen] entering\n");
	
	// Get arguments from running PCB
	int id = running->syscall_args[0];
	int mode = running->syscall_args[1];
	unsigned int value = running->syscall_args[2];

	// Check id consistency
	if(id < 0 || id >= MAX_NUM_SEMAPHORES){
		disastrOS_debug("[semOpen] id not valid\n");
		running->syscall_retvalue=DSOS_ESEMCREATE;
	}
	
	

	// Try to get the idth semaphore
	Semaphore* sem = SemaphoreList_byId(&semaphores_list, id);
	SemDescriptor* des;
	

	// Check if sem_descriptors are too much
	if(running->sem_descriptors.size + 1 > MAX_NUM_SEMDESCRIPTORS_PER_PROCESS){
		disastrOS_debug("[semOpen] max sem descriptors limit exceeded\n");
		running->syscall_retvalue=DSOS_ESEMCREATE;
		return;
	}

	// If DSOS_CREATE try to create the semaphore
	if((mode&DSOS_CREATE) == DSOS_CREATE){
		// If DSOS_CREATE and DSOS_EXCL are specified, then
		// ensure the file is created
		if(sem && (mode&DSOS_EXCL) == DSOS_EXCL){
			disastrOS_debug("[semOpen] DSOS_EXCL mode used, but semaphore already existing\n");
			running->syscall_retvalue=DSOS_ESEMCREATE;
			return;
		}else if(!sem){
			if (semaphores_list.size + 1 > MAX_NUM_SEMAPHORES){
				running->syscall_retvalue=DSOS_ESEMCREATE;
				return;
			}
			sem = Semaphore_alloc(id, value);
			List_insert(&semaphores_list, semaphores_list.last, (ListItem*) sem);
		}
	}

	// If semaphore=NULL something appened: return error
	if(!sem){
		running->syscall_retvalue=DSOS_ESEMCREATE;
		return;
	}

	// Finally create a semaphore descriptor for the running process
	des = SemDescriptor_alloc(running->last_sem_fd, sem, running);
	if (! des){
			disastrOS_debug("[semOpen] something happened while creating the descriptor\n");
			running->syscall_retvalue=DSOS_ESEMNOFD;
			return;
	}

	// Increase last_sem_fd
	running->last_sem_fd++;

	// Create a pointer for the descriptor and insert it 
	// in the process descriptors list
	SemDescriptorPtr* desptr=SemDescriptorPtr_alloc(des);
	List_insert(&running->sem_descriptors, running->sem_descriptors.last, (ListItem*) des);
	des->ptr=desptr;

	// Insert the descriptor in the semaphore descriptors
	List_insert(&sem->descriptors, sem->descriptors.last, (ListItem*) desptr);
	running->syscall_retvalue = des->fd;
}

