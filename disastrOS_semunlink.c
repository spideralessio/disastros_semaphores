#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semUnlink(){
  disastrOS_debug("[semUnlink] entering\n");
  int id=running->syscall_args[0];

  // find the resource in with the id
  Semaphore* sem=SemaphoreList_byId(&semaphores_list, id);
  if (! sem){
    disastrOS_debug("[semUnlink] semaphore %d not existing\n", id);
    running->syscall_retvalue=DSOS_ESEMCLOSE;
    return;
  }

  // ensure the resource is not used by any process
  if(sem->descriptors.size){
    disastrOS_debug("[semUnlink] semaphore %d is opened by other processes, I remove it but not destroy it\n", id);
    sem->id = DSOS_REMOVED_SEM_ID;
    running->syscall_retvalue=0;
    return;
  }

  disastrOS_debug("[semUnlink] semaphore %d removed and destroyed ;) Mission accomplished\n", id);
  sem=(Semaphore*) List_detach(&semaphores_list, (ListItem*) sem);
  assert(sem);
  Semaphore_free(sem);
  running->syscall_retvalue=0;
}
