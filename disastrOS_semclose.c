#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"


// semClose(int fd);
void internal_semClose(){
  // debug don't se nega a nessuno
  disastrOS_debug("[semClose] entering\n");
  
  // Get arguments from running PCB
  int fd = running->syscall_args[0];

  SemDescriptor* des = SemDescriptorList_byFd(&running->sem_descriptors, fd);
  if(! des){
  	disastrOS_debug("[semClose] no semaphore with fd=%d\n", fd);
  	running->syscall_retvalue=DSOS_ESEMCLOSE;
  	return;
  }

  des = (SemDescriptor*) List_detach(&running->sem_descriptors, (ListItem*) des);
  assert(des);

  Semaphore* sem = des->semaphore;

  SemDescriptorPtr* desptr = (SemDescriptorPtr*) List_detach(&sem->descriptors, (ListItem*)(des->ptr));
  assert(desptr);


  // If anyone has called semUnlink and all the process closed the semaphore,
  // then destroy the semaphore
  if(sem->id == DSOS_REMOVED_SEM_ID && sem->descriptors.size == 0){
    disastrOS_debug("[semClose] semUnlink was called on this semaphore, i destroy it.\n");
    sem=(Semaphore*) List_detach(&semaphores_list, (ListItem*) sem);
  	assert(sem);
  	Semaphore_free(sem);
  }

  SemDescriptor_free(des);
  SemDescriptorPtr_free(desptr);
  running->syscall_retvalue=0;
}
