#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"

// we need this to handle the sleep state
void sleeperFunction(void* args){
  printf("Hello, I am the sleeper, and I sleep %d\n",disastrOS_getpid());
  while(1) {
    getc(stdin);
    disastrOS_printStatus();
  }
}


void childOpenCloseWithModeErrors(void* args){
  printf("Hello, I am the child function %d\n",disastrOS_getpid());
  printf("I will open and close a semaphore with mode DSOS_CREATE|DSOS_EXCL\n");
  int mode=DSOS_CREATE|DSOS_EXCL;
  unsigned int value = 1;
  int fd = disastrOS_semOpen(1, mode, value);
  printf("[%d] fd=%d , DSOS_ESEMCREATE=%d\n", disastrOS_getpid(), fd, DSOS_ESEMCREATE);
  printf("It is time to sleep a bit\n");
  if(fd < 0)disastrOS_sleep(1);
  else disastrOS_sleep(2);
  int ret = disastrOS_semClose(fd);
  printf("[%d] ret=%d , DSOS_ESEMCLOSE=%d\n", disastrOS_getpid(), ret, DSOS_ESEMCLOSE);
  disastrOS_exit(0);
}

void childWaitPost(void* args){
  printf("Hello, I am the child function %d\n",disastrOS_getpid());
  printf("I will open a semaphore, wait, sleep, post and finally close it\n");
  int mode=DSOS_CREATE;
  int id = 1;
  unsigned int value = 1;
  printf("[%d] opening sem=%d\n", disastrOS_getpid(), id);
  int fd=disastrOS_semOpen(id, mode, value);
  printf("[%d] opened fd=%d\n", disastrOS_getpid(), fd);
  printf("[%d] waiting sem=%d\n", disastrOS_getpid(), id);
  int ret = disastrOS_semWait(fd);
  printf("[%d] waited ret=%d\n", disastrOS_getpid(), ret);
  printf("[%d] sleeping for 3 s\n", disastrOS_getpid());
  disastrOS_sleep(3);
  printf("[%d] posting sem=%d\n", disastrOS_getpid(), id);
  ret = disastrOS_semPost(fd);
  printf("[%d] posted ret=%d\n", disastrOS_getpid(), ret);
  printf("[%d] closing sem=%d\n", disastrOS_getpid(), id);
  disastrOS_semClose(fd);
  disastrOS_exit(0);
}

void childWaitPostWith2Sems(void* args){
  printf("Hello, I am the child function %d\n",disastrOS_getpid());
  printf("I will open a semaphore, wait, sleep, post and finally close it\n");
  int mode=DSOS_CREATE;
  int id = disastrOS_getpid()%2;
  unsigned int value = disastrOS_getpid()%2 + 1;
  printf("[%d] opening sem=%d\n", disastrOS_getpid(), id);
  int fd=disastrOS_semOpen(id, mode, value);
  printf("[%d] opened fd=%d\n", disastrOS_getpid(), fd);
  printf("[%d] waiting sem=%d\n", disastrOS_getpid(), id);
  int ret = disastrOS_semWait(fd);
  printf("[%d] waited ret=%d\n", disastrOS_getpid(), ret);
  printf("[%d] sleeping for 3 s\n", disastrOS_getpid());
  disastrOS_sleep(3);
  printf("[%d] posting sem=%d\n", disastrOS_getpid(), id);
  ret = disastrOS_semPost(fd);
  printf("[%d] posted ret=%d\n", disastrOS_getpid(), ret);
  printf("[%d] closing sem=%d\n", disastrOS_getpid(), id);
  disastrOS_semClose(fd);
  disastrOS_exit(0);
}
void childTestMaxNumSemaphores(void* args){
  printf("Hello, I am the child function %d\n",disastrOS_getpid());
  printf("I will open a semaphore, wait, sleep, post and finally close it\n");
  int mode=DSOS_CREATE|DSOS_EXCL;
  int id, fd, ret;
  int arr[MAX_NUM_SEMDESCRIPTORS_PER_PROCESS+1] = {-1};
  for (id = 0; id < MAX_NUM_SEMDESCRIPTORS_PER_PROCESS+1; id++){
    fd=disastrOS_semOpen(id, mode, id);
    printf("[%d] opened id=%d, fd=%d\n", disastrOS_getpid(), id, fd);
    if (fd < 0) printf("[%d] got error %d (%s=%d)\n", disastrOS_getpid(), fd, "DSOS_ESEMCREATE", DSOS_ESEMCREATE);
    arr[id] = fd;
  }
  for (id = 0; id < MAX_NUM_SEMDESCRIPTORS_PER_PROCESS+1; id++){
    fd = arr[id];
    ret = disastrOS_semClose(fd);
    printf("[%d] closed fd=%d, ret=%d\n", disastrOS_getpid(), fd, ret);
    int ret = disastrOS_semUnlink(id);
    printf("[%d] unlinked id=%d, ret=%d\n", disastrOS_getpid(), id, ret);
  }
  disastrOS_exit(0);
}

void childOpensTwiceSemaphoreAndUnlink(void* args){
  printf("Hello, I am the child function %d\n",disastrOS_getpid());
  printf("I will open a semaphore twice, close both descriptors and unlink bothdescriptors\n");
  int mode=DSOS_CREATE;
  int id = 10;
  unsigned int value = 1;

  printf("[%d] opening sem=%d\n", disastrOS_getpid(), id);
  int fd1=disastrOS_semOpen(id, mode, value);
  printf("[%d] opened fd=%d\n", disastrOS_getpid(), fd1);
  printf("[%d] opening sem=%d\n", disastrOS_getpid(), id);
  int fd2=disastrOS_semOpen(id, mode, value);
  printf("[%d] opened fd=%d\n", disastrOS_getpid(), fd2);

  printf("[%d] unlink sem=%d\n", disastrOS_getpid(), id);
  int ret = disastrOS_semUnlink(id);
  printf("[%d] unlinked ret=%d\n", disastrOS_getpid(), ret);


  printf("[%d] closing fd=%d\n", disastrOS_getpid(), fd1);
  ret = disastrOS_semClose(fd1);
  printf("[%d] closed ret=%d\n", disastrOS_getpid(), ret);
  printf("[%d] closing fd=%d\n", disastrOS_getpid(), fd2);
  ret = disastrOS_semClose(fd2);
  printf("[%d] closed ret=%d\n", disastrOS_getpid(), ret);
  disastrOS_exit(0);
}

void testFN(void (*funcp)(void*), int threads, void* args){
  printf("I feel like to spawn %d nice threads\n", threads);
  int alive_children=0;
  for (int i=0; i<threads; ++i) {
    disastrOS_spawn(funcp, 0);
    alive_children++;
  }

  disastrOS_printStatus();
  int retval;
  int pid;
  while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){ 
    printf("initFunction, child: %d terminated, retval:%d, alive: %d \n",
     pid, retval, alive_children);
    --alive_children;
    disastrOS_printStatus();
  }

  printf("I think it is time to clean up my children mess\n");
  
  ListItem* aux=semaphores_list.first;
  while(aux){
    Semaphore* r=(Semaphore*)aux;
    disastrOS_semUnlink(r->id);
    aux=aux->next;
  }
  disastrOS_printStatus();
}

void initFunction(void* args) {
  disastrOS_printStatus();
  printf("hello, I am init and I just started\n");
  disastrOS_spawn(sleeperFunction, 0);

  printf("\n\nTest DSOS_CREATE/DSOS_EXCL mode\n\n\n");
  testFN(childOpenCloseWithModeErrors, 3, args);
  printf("\n\nTest wait and post with 1 sem and 4 threads\n\n\n");
  testFN(childWaitPost, 4, args);
  printf("\n\nTest wait and post with 2 sems and 8 threads\n\n\n");
  testFN(childWaitPostWith2Sems, 8, args);
  printf("\n\nTest max num sems errors with 1 thread\n\n\n");
  testFN(childTestMaxNumSemaphores, 1, args);
  printf("\n\nTest 1 sem opened twice by same thread and unlink\n\n\n");
  testFN(childOpensTwiceSemaphoreAndUnlink, 1, args);
  printf("shutdown!");
  disastrOS_shutdown();
}

int main(int argc, char** argv){
  char* logfilename=0;
  if (argc>1) {
    logfilename=argv[1];
  }
  // we create the init process processes
  // the first is in the running variable
  // the others are in the ready queue
  
  // spawn an init process
  printf("start\n");
  disastrOS_start(initFunction, 0, logfilename);
  return 0;
}
