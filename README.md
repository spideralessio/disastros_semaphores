# DisastrOS implementation of Semaphores


In this folder

- stub gently given by the profs with some corrections.

- semaphore syscalls:

  1. disastrOS_semOpen: create semaphore and open descriptor
  2. disastrOS_semWait: decrease semaphore value and lock processes
  3. disastrOS_semPost: increase semaphore value and unlock processes
  4. disastrOS_semClose: close semaphore descriptor
  5. disastrOS_semUnlink: remove semaphore and destroy semaphore after every process closed it