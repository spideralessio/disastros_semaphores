#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"


// semPost(int fd);
void internal_semPost(){
  disastrOS_debug("[semPost] entering\n");
  
  int fd = running->syscall_args[0];

  SemDescriptor* des = SemDescriptorList_byFd(&running->sem_descriptors, fd);
  SemDescriptorPtr* desptr;
  if(! des){
  	running->syscall_retvalue=DSOS_ESEMPOST;
  }

  Semaphore* sem = des->semaphore;
  assert(sem);

  sem->count++;
  if(sem->count>0){
  	if(sem->waiting_descriptors.first){
  		desptr = (SemDescriptorPtr*) List_detach(&sem->waiting_descriptors, (ListItem*) sem->waiting_descriptors.first);
  		desptr = (SemDescriptorPtr*) List_insert(&sem->descriptors, sem->descriptors.last, (ListItem*) desptr);
  		des = desptr->descriptor;
  		disastrOS_debug("[semPost] unlocked pcb %d\n", des->pcb->pid);
  		sem->count--;
  		List_detach(&waiting_list, (ListItem*) des->pcb);
  		List_insert(&ready_list, ready_list.last, (ListItem*) des->pcb);
  		des->pcb->status = Ready;
  		des->pcb->syscall_retvalue = 0;
  	}else{
  		disastrOS_debug("[semPost] there is no one to unlock\n");
  	}
  }
	disastrOS_debug("[semPost] semaphore %d posted, everything went right\n", sem->id);
	//disastrOS_printSemaphore(semnum);
}
